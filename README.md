# gr-newline

This GNU Radio Block adds a new line symbol to a Async Message sequence. <br/>
<br/>
Installation is CMake based so <br/>
<br/>
cd gr-newline/ <br/>
mkdir build && cd build <br/>
cmake .. <br/>
make <br/>
sudo make install <br/>
sudo ldconfig <br/>
<br/>
should suffice when using Ubuntu 18.04 LTS.
