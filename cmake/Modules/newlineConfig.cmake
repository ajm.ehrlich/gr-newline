INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_NEWLINE newline)

FIND_PATH(
    NEWLINE_INCLUDE_DIRS
    NAMES newline/api.h
    HINTS $ENV{NEWLINE_DIR}/include
        ${PC_NEWLINE_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    NEWLINE_LIBRARIES
    NAMES gnuradio-newline
    HINTS $ENV{NEWLINE_DIR}/lib
        ${PC_NEWLINE_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NEWLINE DEFAULT_MSG NEWLINE_LIBRARIES NEWLINE_INCLUDE_DIRS)
MARK_AS_ADVANCED(NEWLINE_LIBRARIES NEWLINE_INCLUDE_DIRS)

